<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::all();
        $posts=Post::orderBy('created_at', 'desc')->paginate(6);
        return view('post.index', compact ('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        
        $a=new Post();
        $a->title=$request->input('title');
        $a->body=$request->input('body');
        $a->img = $request->file('img')->store('/public/img');
        $a->user_id=Auth::user()->id;
       
        
       
        
            
            dispatch(new ResizeImage(
                $a->img,
                150,
                150
            ));

            dispatch(new ResizeImage(
                $a->img,
                400,
                250
            ));

            dispatch(new ResizeImage(
                $a->img,
                600,
                600
            ));

     $a->save(); 
            
         
        
       
        return redirect (route('post.index'))->with('message', 'Post Pubblicato!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }

    public function showBig(Post $post)
    {
        return view('post.showBig', compact('post'));
    }

    
    public function showBigger(Post $post)
    {
        return view('post.showBigger', compact('post'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title=$request->input('title');
        $post->body=$request->input('body');
        $post->img=$request->file('img')->store('/public/img');
        $post->save();
        return redirect (route('post.index'))->with('message', 'Post Modificato!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        Storage::delete($post->img);
        $post->delete();
        return redirect (route('post.index'))->with('message', 'Post Eliminato!');
    }

    public function PostForUserId($user_id)
    {
        $posts = User::find($user_id)->posts;
        return view ('post.userindex', compact('posts'));
    }
}
