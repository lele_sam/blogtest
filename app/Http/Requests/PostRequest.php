<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'min:6',
            'img' => 'required',
            'body' => 'required',
        ];
    }

    public function messages()
    {
        return[
            'title.required' => 'Il Titolo è obbligatorio',
            'title.min' => 'Il Titolo deve essere di almeno 6 caratteri',
            'img.required' => 'La foto è obbligatoria',
            'body.required' => 'La descrizione è obbligatoria',
        ];
    }
}
