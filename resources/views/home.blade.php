<x-app>
    
    <div class="container-fluid" id="header">
        <div class="row text-center">
            <div class="col-12">
                <h1 class="my-5 py-5">Test Brownie</h1>
            </div>
        </div>
    </div>
    
    <div class="container mb-5 pb-5">
        <div class="row">
            <div class="col-12">
                @if (Auth::user())
                <h3>Benvenuto <span class="text-capitalize">{{Auth::user()->name}}</span></h3>
                @else
                <div class="d-none">registrati</div>
                @endif
            </div>
        </div>
    </div>


    
    <div class="container-fluid my-5">
        <div class="row text-center my-5">
            <div class="col-12">
                <a href="{{route('post.create')}}" class="btn-home my-5">Crea Post</a>
            </div>
        </div>
        <div class="row text-center my-5">
            <div class="col-12">
                <a href="{{route('post.index')}}" class="btn-home my-5">Tutti i Post</a>
            </div>
        </div>
        <div class="row text-center my-5">
            <div class="col-12">
                @if (Auth::user())
                <a href="{{route('post.for.user', ['user_id'=> Auth::user()->id])}}" class="btn-home my-5">Guarda i Tuoi Post</a>
                @else
                <div class="d-none">registrati</div>
                @endif
                
            </div>              
        </div>
    </div>



</x-app>

