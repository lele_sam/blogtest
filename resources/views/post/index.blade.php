use Illuminate\Support\Facades\Storage;
<x-app>
    <div class="container-fluid" id="header">
        <div class="row text-center">
            <div class="col-12">
                <h1 class="mt-5 pt-5">Guarda Tutti i Post</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{route('post.create')}}" class="btn bg-danger text-white text-uppercase py-2 px-5">Crea</a>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row d-flex justify-content-center">
            
            @foreach ($posts as $post)
            <div class="col-12 col-lg-6">
                <div class="card bg-danger my-5 mx-3">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <a href="{{route('post.edit', compact('post'))}}" class="btn-mod text-right mx-3 my-2 text-white my-2"><i class="fas fa-wrench ml-1 my-2"></i> Modifica </a>
                        </div>
                        <div class="col-6">
                            <form method="POST"
                            action="{{route('post.destroy', compact('post'))}}">
                            @csrf
                            @method('delete')
                            <button type="submit" class=" btn-delete mx-3 text-white float-right bg-danger my-2"><i class="fas fa-trash ml-1"></i> Elimina</button>
                        </form>
                    </div>
                </div>
                <h4 class="card-title text-center my-3">{{$post->title}}</h4>
                <p class="m-2">Pubblicato da <span class="text-capitalize">{{$post->user->name}}</span></p>
                <a href="{{route('post.show', compact('post'))}}" class="btn-index mx-auto my-3">Scopri di più</a>
            </div>
        </div>
        
        @endforeach
       
    </div>


<div class="pagination d-flex align-items-center justify-content-center mb-5 pb-5">
    {{$posts->links()}}

</div>


</x-app>

