<x-app>

    
    <div class="container my-5 py-5">
        <div class="row">
            <div class="col-12">
                <a href="{{route('post.index', compact('post'))}}">torna indietro</a>
            <div class="text-center">
                <img id="img" src="{{Storage::url($post->img)}}" class="img-fluid mx-auto mt-2" alt="">
            </div> 
            </div>
        </div>
    </div>

    <script>
        let src = document.getElementById('img').src
       let srcFinal = src.replace(/img/,"img/crop600x600_").split("_/").join("_")
        window.onload=function(){
    document.getElementById('img').src= srcFinal;
}
    </script>

</x-app>