<x-app>

    <a href="{{route('post.edit', compact('post'))}}" class="btn-mod float-right p-2 mx-5 my-4 text-white my-2"><i class="fas fa-wrench ml-1 my-2"></i> Modifica </a>

    <div class="container my-5 py-5">
        <div class="row">
            <div class="col-12">
            <h1 class="text-center">{{ $post->title }}</h1>
            <p class="lead mt-5"> {{ $post->body }}</p>
            <div class="text-center">
                <img id="img" src="{{Storage::url($post->img)}}" class="img-fluid mx-auto mt-2" alt="">
            </div>
            <a href="{{route('post.showBig', compact('post'))}}">guarda foto più grande</a> <br>
            <a href="{{route('post.showBigger', compact('post'))}}">guarda foto ancora più grande</a>
            </div>
        </div>
    </div>

    <script>
        let src = document.getElementById('img').src
       let srcFinal = src.replace(/img/,"img/crop150x150_").split("_/").join("_")
        window.onload=function(){
    document.getElementById('img').src= srcFinal;
}
    </script>

</x-app>