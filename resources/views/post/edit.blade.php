<x-app>

@if ($errors->any())
    <div class="alert alert-danger bg-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container">
        <div class="row text-center">
            <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{route('post.update', compact('post'))}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                    <div class="form-group">
                      <label>Titolo: </label>
                    <input name="title" type="text" class="form-control" value="{{$post->title}}">
                    </div>
                    <div class="form-group mt-5 mb-3">
                      <label>Carica Immagine: </label>
                      <input type="file" name="img" value="{{Storage::url($post->img)}}">
                  </div>
                    <div class="form-group">
                      <label>Scrivi Qui il Tuo Post: </label> <br>
                      <textarea name="body" rows="4" cols="60">{{$post->body}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-custom">Pubblica</button>
                  </form>
            </div>
        </div>
    </div>


</x-app>