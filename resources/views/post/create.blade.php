<x-app>
    @if (Auth::user())
    <div class="container my-5 py-5">
        <div class="row text-center">
            <div class="col-12">
                <h2>Crea il Tuo Post</h2>
            </div>
        </div>
    </div>

@if ($errors->any())
    <div class="alert alert-danger bg-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container">
        <div class="row text-center">
            
            <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{route('post.store')}}" enctype="multipart/form-data">
                @csrf
                
                    <div class="form-group">
                      <label>Titolo: </label>
                      <input name="title" type="text" class="form-control">
                    </div>
                    <div class="form-group mt-5 mb-3">
                        <label>Carica Immagine: </label>
                        <input type="file" name="img">
                    </div>
                    <div class="form-group">
                        <label>Descrizione: </label>
                        <input name="body" type="text" class="form-control">
                      </div>
                    <button type="submit" class="btn btn-custom">Pubblica</button>
                  </form>
            </div>
        </div>
    </div>
  
    @else
   <div class="container my-5 py-5">
       <div class="row">
           <div class="col-12 text-center">
            <a href="{{route('home')}}" class="text-white">Registrati per poter postare</a>
           </div>
       </div>
   </div>
    @endif
    
</x-app>

