<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/post/index', [PostController::class, 'index'])->name('post.index');

Route::get('/post/show/{post}', [PostController::class, 'show'])->name('post.show');

Route::get('/post/showpic/{post}', [PostController::class, 'showBig'])->name('post.showBig');

Route::get('/post/showpicbigger/{post}', [PostController::class, 'showBigger'])->name('post.showBigger');

Route::get('/post/edit/{post}', [PostController::class, 'edit'])->name('post.edit');

Route::get('/post/create', [PostController::class, 'create'])->name('post.create');

Route::post('/post/store', [PostController::class, 'store'])->name('post.store');

Route::put('/post/update/{post}', [PostController::class, 'update'])->name('post.update');

Route::delete('/post/destroy/{post}', [PostController::class, 'destroy'])->name('post.destroy');

Route::get('/post/user/{user_id}', [PostController::class, 'PostForUserId'])->name('post.for.user');

